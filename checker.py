#!/usr/bin/python3

import os
from hashlib import sha256
from sys import argv
import socket
from random import choice
from string import ascii_uppercase



STATUS_CODE = {
    'INVALID ARGVS': 110,
    'SUCCESS': 101,
    'CORRUPT': 102,
    'MUMBLE': 103,
    'DOWN': 104,
}

PORT = {
    'smtp': 25,
    'pop3': 110,
    'ereg': 9000,
}

GOOD_ANSW = {
    'smtp': b'220 smtp server is ready!',
    'pop3': b'+OK pop3 server ready!',
    'ereg': b'+OK ereg server is ready!',
}


def send_mail(send_from, send_to, subject, text, server, files=None):
    import smtplib
    from os.path import basename
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.utils import COMMASPACE, formatdate

    assert isinstance(send_to, list)

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)


    smtp = smtplib.SMTP(server)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def sha(string):
    return sha256(string.encode("utf-8")).hexdigest()


def check(hostname):
    try:
        response = os.system("ping -c 1 %s > /dev/null 2>&1" % hostname)
        if response != 0:
            print("Host unreachable!")
            return STATUS_CODE['DOWN']
        for i in PORT:
            sock = socket.socket()
            sock.connect((hostname, PORT[i]))
            if GOOD_ANSW[i] not in sock.recv(256):
                print("Bad hello answer!")
                return STATUS_CODE['MUMBLE']

        print("Ok")
        return STATUS_CODE['SUCCESS']
    except Exception as e:
        print('Exception: ', e)
        return STATUS_CODE['CORRUPT']


def put(hostname, id, flag):
    # try:
    users = (('user_%s1' % str(id), sha(str(id))[:32]),
             ('user_%s2' % str(id), sha(str(id))[32:]))


    sock = socket.socket()
    sock.connect((hostname, PORT['ereg']))
    sock.recv(256)
    for i in users:
        if (ord(str(id)[-1]) % 3 == 1) and (i == users[-1]):
            sock.send(('CREATE %s %s' % (i[0], i[-1][:2])).encode('utf-8'))
            if b'-ERR' in sock.recv(256):
                sock.send(('CREATE %s %s' % i).encode('utf-8'))
                sock.recv(256)
        else:        
            sock.send(('CREATE %s %s' % i).encode('utf-8'))
            sock.recv(256)

    sock.send(b'LIST\r\n')
    data = sock.recv(1024)
    for i in users:
        if i[0] not in data.decode("utf-8"):
            print('Command LIST works bad')
            return STATUS_CODE['MUMBLE']

    fromaddr = '%s@%s' % (users[0][0], hostname)
    toaddr = ['%s@%s' % (users[1][0], hostname)]
    
    if not ord(str(id)[-1]) % 3:
        f = open('mails/%s.txt' % users[0][0], 'w')
        f.write("It's my flag:\n%s" % flag)
        f.close()
        os.popen("zip --password %s mails/%s.zip mails/%s.txt" % (
            sha(str(id))[32:], 'user_%s1' % str(id), 'user_%s1' % str(id))).read()
        # os.popen("zip --password %s mails/%s.zip mails/%s.txt" % (
        #     sha(str(id))[32:], 'user_%s1' % str(id), 'user_%s1' % str(id)))
        send_mail(fromaddr,
                  toaddr, 
                  'flag', 
                  'eahh', 
                  hostname,
                  files = ['mails/%s.zip' % ('user_%s1' % str(id))])
        os.remove('mails/%s.txt' % users[0][0])
        os.remove('mails/%s.zip' % users[0][0])
    else:
        send_mail(fromaddr,
                  toaddr, 
                  'flag', 
                  "It's my flag:%s\n" % flag,
                  hostname)

    # except Exception as e:
    #     print('Exception: ', e)
    #     return STATUS_CODE['CORRUPT']

    print("Ok")
    return STATUS_CODE['SUCCESS']


def get(hostname, id, flag):
    import poplib
    from base64 import b64decode
    import re
    import os


    flag = flag.encode("utf-8")

    try:
        M = poplib.POP3(hostname)
        M.user('user_%s2' % str(id))
        if ord(str(id)[-1]) % 3 == 1:
            try:
                M.pass_(sha(str(id))[32:34])
            except:
                M.pass_(sha(str(id))[32:])
        else:
            M.pass_(sha(str(id))[32:])
        for i in M.list()[1][::-1]:
            mail = M.retr(i.decode('utf-8').split(" ")[0])[1]
            if not ord(str(id)[-1]) % 3:
                mail = b"\n".join(mail[:-2]).decode("utf-8").split("\n\n")[-1]
                f = open('mails/user_%s2.zip' % str(id), 'wb')
                f.write(b64decode(mail.replace("\n","")))
                f.close()
                os.popen("unzip -P %s mails/%s.zip" % (sha(str(id))[32:], 'user_%s2' % str(id))).read()
                # os.popen("unzip -P %s mails/%s.zip" % (sha(str(id))[32:], 'user_%s2' % str(id)))
                f = open('mails/user_%s1.txt' % str(id), 'r')
                r = f.read() 
                if flag.decode("utf-8") in r:
                    os.remove('mails/user_%s1.txt' % str(id))
                    os.remove('mails/user_%s2.zip' % str(id))

                    os.popen('ssh root@10.53.254.161 "python3 /home/udalenka/'
                          'create_dk.py execute %s %s %s"' %(
                            hostname, 'user_%s2' % str(id), sha(str(id))[32:]))

                    print('Ok')
                    return STATUS_CODE['SUCCESS']
                else:
                    os.remove('mails/user_%s1.txt' % str(id))
                    os.remove('mails/user_%s2.zip' % str(id))
                 
            elif flag in b"\n".join(mail):
                print("Ok")
                return STATUS_CODE['SUCCESS']
    except Exception as e:
        print('Exception: ', e)
        return STATUS_CODE['CORRUPT']

    print("Flag not found!")
    return STATUS_CODE['MUMBLE']


if __name__ == '__main__':
    if len(argv) > 1:
        if argv[1] == "check":
            if len(argv) > 2:
                exit(check(argv[2]))
        elif argv[1] == "put":
            if len(argv) > 4:
                exit(put(argv[2], argv[3], argv[4]))
        elif argv[1] == "get":
            if len(argv) > 4:
                exit(get(argv[2], argv[3], argv[4]))
    print('Invalid argvs')
    exit(STATUS_CODE['INVALID ARGVS'])
