#!/usr/bin/python3

import docker
from sys import argv 

def execute(hostname, user, passw):
    client = docker.DockerClient(base_url='unix://var/run/docker.sock')
    image = client.images.build(path='/home/udalenka/dk_script', dockerfile='/home/udalenka/dk_script/Dockerfile')

    try:
        id_d = client.containers.run(image = image, command = 'python3 script.py %s %s %s' % (hostname, user, passw), detach = True, mem_limit = '50m')
        lo = id_d.logs(follow = True)
        print(lo)
        id_d.remove()

    except Exception as e:
        print(e)


if __name__ == '__main__':
    if argv[1] == "execute":
        exit(execute(argv[2], argv[3], argv[4]))
    exit('Exit')
