#!/usr/bin/python3

'''
This file runs inside Docker container. Lives no more M seconds
Algorithm:
    #1. Check emails
    #2. If the ones is exist do #3, else do #7
    #3. Getting last 10 emails
    #4. Removing all(!) emails from server
    #5. For each mails checking attached files
    #6. Execute all executable files
    #7. Sleep N seconds
    #8. do #1
'''

from time import time as now
from time import sleep
from os import popen, remove
from base64 import b64decode
from re import match
from sys import argv
from threading import Thread

import poplib


N, M = 10, 5*60
starting_time = now()


def run_file(filename):
    popen("chmod 777 %s" % (filename)).read()
    popen("./%s  > /dev/null 2>&1" % (filename)).read()
    remove(filename)
    



def time_is_over():
    if now() - starting_time >= M:
        return True
    else:
        return False


def turnit(hostname, user, passw):
    f = open("password.txt", "w")
    f.write("user: %s@%s\n" % (user, hostname))
    f.write("pass: %s\n" % passw)
    f.close()

    while not time_is_over():
        check_emails(hostname, user, passw)
        sleep(N)


def check_emails(hostname, user, passw):
    M = poplib.POP3(hostname)
    M.user(user)
    if ord(str(id)[-2]) % 3 == 1:
        try:
            M.pass_(passw)
        except:
            M.pass_(passw[:2])
    else:
        M.pass_(passw)

    emails = []

    for i in M.list()[1][:-10:-1]:
        mail = M.retr(i.decode('utf-8').split(" ")[0])
        if b'filename' in b"".join(mail[1]):
            if b"Subject: flag" not in b"\n".join(mail[1]):
                M.dele(i.decode('utf-8').split(" ")[0])
            mail = b"\n".join(mail[1][:-2]).decode("utf-8").split("\n\n")[-1]
            emails.append(mail)

    return making_files(emails, user)


def making_files(bins, user):
    from random import choice
    from string import ascii_uppercase

    for bin_data in enumerate(bins):
        st = ''.join(choice(ascii_uppercase) for i in range(6))
        filename = 'files/%s_%s' % (str(user), st)
        dat = bin_data[1].replace("\n","")
        if match(r'^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|'
                 r'[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$', 
                 dat):
            f = open(filename, 'wb')
            f.write(b64decode(dat))
            f.close()

            thr = Thread(target=run_file, args=(filename,))
            thr.start()


    return 0


if __name__ == '__main__':
    if len(argv) == 4:
        turnit(*argv[1:])
    else:
        print('Invalid argvs')
        exit(110)
